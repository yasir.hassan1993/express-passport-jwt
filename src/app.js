const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const mongoose = require('mongoose');
const passport = require('passport');
require('dotenv').config();

mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`);
mongoose.connection.on('error', (error) => console.log(error));
mongoose.Promise = global.Promise;

require('./auth/auth');
const UserModel = require('./models/user.model');
const { notFound, errorHandler } = require('./middlewares/errorhandlers');
const routes = require('./routes/routes');
const secureRoute = require('./routes/secure-routes');

const app = express();

app.use(morgan('common'));
app.use(helmet());
app.use(express.json());

app.get('/', (req, res) => {
  res.json({
    message: '🦄🌈✨👋🌎🌍🌏✨🌈🦄',
  });
});

app.use('/auth', routes);
app.use('/user', passport.authenticate('jwt', { session: false }), secureRoute);

app.use(notFound);
app.use(errorHandler);

module.exports = app;
