# Express Passport JWT

Testing out Passport Local and Passport JWT authentication methods. 

## Local Dev

To test out locally, copy .env.sample file to .env and add the necessary credentials.
After that run `npm run dev`.